mkdir reports
call mvn verify  -Dmaven.test.failure.ignore=true -DsystemPropertiesFile=serenity_chrome.properties
robocopy target reports\chrome-win8 /MIR
call mvn verify  -Dmaven.test.failure.ignore=true  -DsystemPropertiesFile=serenity_ie.properties
robocopy target reports\iexplorer-win8_1 /MIR
call mvn verify  -Dmaven.test.failure.ignore=true  -DsystemPropertiesFile=serenity_firefox.properties
robocopy target reports\firefox-win8 /MIR
call mvn verify  -Dmaven.test.failure.ignore=true  -DsystemPropertiesFile=serenity_safari.properties
robocopy target reports\safari-mac10 /MIR
call mvn verify  -Dmaven.test.failure.ignore=true  -DsystemPropertiesFile=serenity_edge.properties
robocopy target reports\MsEdge-win10 /MIR
