Narrative:
As a user in the PSFT
I want to create a class
So that i can access and verify the class in D2L
					 
Scenario:  Create a class in PSFT and verify in D2L

Given user is on the PSFT login page
When user enters <userName> and <pwd>
Then user clicks on submit button
When user clicks on staff portal and HR/SA administration
When user navigates to schedule new course
Then user enter <term>, <subArea>, <catalogNbr>, <campus>, <courseId> details
And user clicks on search button
And user clicks on save button
Then user logout of psft
When user is on D2L login <pageURL>
Then user login into D2L using <d2lUserName> and <d2lPassword>
When user is on home page click on search class
Then user enters class details to verify class creation


Examples:
|userName||pwd||term||subArea||catalogNbr||campus||courseId||pageURL||d2lUserName||d2lPassword|
|sbhupathiraju||Model$12345||2181||CBR||600||ADLPH||027663||https://learnqa.umuc.edu||dgianna1||TestPw1|