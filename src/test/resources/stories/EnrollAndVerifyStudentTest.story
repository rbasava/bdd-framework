Narrative:
As a user in the PSFT
I want to enroll a student to a class
So that i can access the classlist and verify the enrolled student in D2L
				 
Scenario:  Enroll a student in PSFT and verify in D2L

Given user is on the PSFT login page
When user enters <userName> and <pwd>
Then user clicks on submit button
When user clicks on staff portal and HR/SA administration
When user navigates to enroll a student
Then user enter <studentID>, <acadCareer>, <term> details and click add
And user enters <classNbr>, overrides and save details
And user saves notify membership details
Then user logsout of psft
When user is on D2L login <pageURL>
Then user login into D2L using <d2lUserName> and <d2lPassword>
When user is on home page click on search class
Then user enters class details to verify class creation
And user navigates to classlist to verify created user


Examples:
|userName||pwd||term||subArea||catalogNbr||campus||courseId||pageURL||d2lUserName||d2lPassword||studentID||acadCareer||classNbr|
|sbhupathiraju||Model$12345||2181||CBR||600||ADLPH||027663||https://learnqa.umuc.edu||dgianna1||TestPw1||1135000||GR4T||1230|