package com.umuc.serenity.steps;

import com.umuc.serenity.pages.UMUCLandingPage;
import com.umuc.serenity.pages.UMUCLoginPage;
import com.umuc.serenity.pages.BasePage;

import net.thucydides.core.annotations.Step;
import net.thucydides.core.steps.ScenarioSteps;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertNotNull;

import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;

import com.umuc.serenity.pages.CreateAndVerifyPage;


/*
 * 
 * This is a Steps class. It contains the actual test cases.
 */
public class CreateAndVerifyClassStepsTest extends ScenarioSteps {

	UMUCLandingPage landingPage;
//	UMUCLoginPage loginPage;
	BasePage basePage;
	CreateAndVerifyPage enrollPage;
	
	public static String classNumber = "";
	public static String classSection = "";
	
	/*
	 * The way the tests are run are:
	 * 1. The stories are read and the steps are extracted.
	 * 2. The Steps classes are read the steps are extracted.
	 * 3. For each step in a given story the corresponding steps from the Steps 
	 * 		classes are matched up.
	 *    If no match is found the test is marked PENDING.
	 *    Otherwise the step is executed. The execution may succeed or fail.
	 *    Note that this is why the text in the @Given, @When, and @Then _MUST 
	 *    MATCH EXACTLY_ the corresponding step in the story.
	 *    
	 */
	@Step
	@Given("user is on the PSFT login page")
	public void open_landing_page() {
		//assertThat(landingPage.getUMUCLogo().isDisplayed()).isTrue();		
		landingPage.open();
		
	}

	@Step
	@When("user enters $userName and $pwd")
	public void enter_username_password(String userName, String pwd) {
		// Nothing to do here
		landingPage.enterPSFTUserName(userName);
		landingPage.enterPSFTPassword(pwd);
	}

	@Step
	@Then("user clicks on submit button")
	public void click_submit_button() {
	
		landingPage.clickSubmitButton();
	}
	
	@Step
	@When("user clicks on staff portal and HR/SA administration")
	public void click_staff_portal_and_hrsa_admin_link() {
		// Nothing to do here
		//System.out.println("Waiting for the page to load.");
		landingPage.clickStaffAndHRSALinks();		
	}
	
	@Step
	@When("user navigates to schedule new course")
	public void naviaget_to_schedule_new_course() {
		
		landingPage.navigateToScheduleNewCourse();
	}
	
	
	@Step
	@Then("user enter $term, $subArea, $catalogNbr, $campus, $courseId details")
	public void enter_search_criteria(String term, String subArea, String catalogNbr, String campus,
			 String courseId) {
		System.out.println("In Search Page");
		landingPage.enterSearchCriteia(term, subArea, catalogNbr, campus, courseId);
	}
	
	@Step
	@Then("user clicks on search button")
	public void click_search_button() {
		
		classNumber =  landingPage.clickClassSearchButton();
		//landingPage.clickClassSearchButton();
		System.out.println(classNumber);
	}
	
	@Step
	@Then("user clicks on save button")
	public void click_class_save_button() {
	
		landingPage.clickClassSaveButton();
	}
	
	@Step
	@Then("user logout of psft")
	public void click_logout_psft() {
	
		landingPage.clickPSFTLogout();
	}
	
	@Step
	@When("user is on D2L login $pageURL")
	public void launch_D2L(String pageURL) {
		
		landingPage.launchD2L(pageURL);
	}
	
	@Step
	@Then("user login into D2L using <d2lUserName> and <d2lPassword>")
	public void login_D2L(String d2lUserName, String d2lPassword) {
		
		landingPage.enterUserName(d2lUserName);
		landingPage.enterPassword(d2lPassword);
		landingPage.loginD2L();
		 
	}
	
	@Step
	@When("user is on home page click on search class")
	public void enter_class_details() {
		//assertThat(basePage.ssoErrorNavigationPage(errorPageURL)).isTrue();
		//basePage.checkErrorPage();
		landingPage.clickSearchD2LClass();;
	}
	
	@Step
	@Then("user enters class details to verify class creation")
	public void search_and_verify_class() {
		
		landingPage.SearchAndVerifyD2LClass("CBR", "600", "5555", "2181");
		
	}
	
}
