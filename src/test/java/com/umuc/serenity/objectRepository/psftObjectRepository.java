package com.umuc.serenity.objectRepository;

import org.openqa.selenium.By;

public class psftObjectRepository {

	public static By submit_button = By.xpath("//input[@value='Submit']");
	public static By psft_toStaffPortal=By.linkText("To Staff Portal");
	public static By psft_Hrsa_Administrator=By.linkText("HR/SA Administration");
	public static By psft_Main_Menu = By.xpath("//a[contains(text(),'Main Menu')]");
	public static By psft_Curr_Mgmt = By.linkText("Curriculum Management"); 
	
			//By.xpath("//li[@class='HCSR_CURRICULUM_MANAGEMENT']/following-sibling::div[@class='ptnav2toggle']");
	public static By psft_Schedule_Classes = By.linkText("Schedule of Classes");
			//By.xpath("//li[@class='HCSR_SCHEDULE_OF_CLASSES']/following-sibling::div[@class='ptnav2toggle']");
	public static By psft_Schedule_New_Course = By.partialLinkText("Schedule New Course");
	
	
	public static By term = //By.id("CRSE_OFFER_SCTY_STRM"); //By.xpath("(//label[text()='Term:']/following::div/input)[1]");
			By.xpath("//*[@id='CRSE_OFFER_SCTY_STRM']");
	public static By subject_area= By.id("CRSE_OFFER_SCTY_SUBJECT"); //By.xpath("(//label[text()='Subject Area:']/following::div/input)[1]");
	public static By catalog_number=By.xpath("(//label[text()='Catalog Nbr:']/following::div/input)[1]");
	public static By campus=By.xpath("(//label[text()='Campus:']/following::div/input)[1]");
	public static By search_button= By.id("#ICSearch"); //By.xpath("//input[@id='#ICSearch']");
	public static By CourseID=By.xpath("(//label[text()='Course ID:']/following::div/input)[1]");
	public static By classNbr= By.id("CLASS_TBL_CLASS_NBR$0"); //By.xpath("(//label[text()='Class Nbr:']/following::div/input)[1]");
	public static By classSection= //By.xpath("(//label[text()='Class Section:']/following::div/input)[1]");
			By.id("CLASS_TBL_CLASS_SECTION$0");
	
	public static By  lblCourseID=By.xpath("//*[@id='CLASS_TBL_SCTY_CRSE_ID']");
	
	
	public static By termScheduleOfClass = By.id("CLASS_TBL_SCTY_STRM");
	public static By subject_areaScheduleOfClass=By.id("CLASS_TBL_SCTY_SUBJECT");
	public static By catalog_numberScheduleOfClass=By.id("CLASS_TBL_SCTY_CATALOG_NBR");
	public static By campusScheduleOfClass=By.id("CLASS_TBL_SCTY_CAMPUS");
	public static By CourseIDScheduleOfClass=By.id("CLASS_TBL_SCTY_CRSE_ID");
	
	public static By  saveClass = By.id("#ICSave");
	
	public static By lnkLogOutOrSingOutInPSFT=By.xpath("//*[text()='Sign out' or text()='Log out' or text()='Log Out']");
	
	public static By d2l_UserName= By.id("username");
	public static By d2l_UserPwd= By.id("password");
	 
	public static By d2l_Login_Button = 
			By.xpath("//input[@value='Login' or @value='LOGIN' or @value='LOG IN' or @value='LOG IN '] | //button[text()='Log In']");
	
	
	public static By d2l_course_search = By.xpath("//button[@class='d2l-navigation-s-button-highlight d2l-dropdown-opener' and @title='Select a course...']");
		
	
	public static By txtSearch = By.xpath("//input[@type='search']");
	public static By btnSearch= By.xpath("//input[@value='Search' and @type='button']");
	public static By lnkClassNameWithClassDetails(String subjectArea, String catalog, String classSection, String term)
	    {
		 
		 String searchString1=subjectArea+" "+catalog+" "+classSection;
		 String searchString2=subjectArea+"  "+catalog+" "+classSection;
	    	By lnkClassNameWithClassDetails=
	    	//By.xpath("//a[(contains(text(),'"+searchString1+"') or contains(text(),'"+searchString2+"')) and contains(text(),'"+term+"')]");
	    			By.xpath("//a[contains(text(),'"+subjectArea+"') and contains(text(),'"+catalog+"') and contains(text(),'"+classSection+"') and contains(text(),'"+term+"')]");
	    	return lnkClassNameWithClassDetails;
	    }
	
	//Enrollment
	public static By psft_Rec_And_Enroll = By.linkText("Records and Enrollment");
	public static By psft_Qucik_Enroll = By.linkText("Quick Enroll a Student");	
	
	public static By psft_studentId = By.id("QUIK_E_ADD_SRCH_EMPLID");
	public static By psft_AcademicCarrier= By.id("QUIK_E_ADD_SRCH_ACAD_CAREER");
	public static By psft_Term= By.id("QUIK_E_ADD_SRCH_STRM");
	public static By psft_Add= By.id("#ICSearch");
	public static By psft_enrlstudentId = By.xpath("//input[@id='ENRL_REQ_HDR_VW_EMPLID']");
	public static By psft_enrlAcademicCarrier= By.xpath("//input[@id='ENRL_REQ_HDR_VW_ACAD_CAREER']");
	public static By psft_enrlTerm= By.xpath("//input[@id='ENRL_REQ_HDR_VW_STRM']");
		


	public static By quickAction = By.id("ENRL_REQ_DETAIL_ENRL_REQ_ACTION$0");
	public static By quickEnroll_ClassNbr = By.id("ENRL_REQ_DETAIL_CLASS_NBR$0");
	public static By generalOverrides = By.xpath("//span[contains(text(), 'General Overrides')]");
	public static By quick_Appointment = By.id("ENRL_REQ_DETAIL_OVRD_APPT$0");
	public static By quick_UnitLoad = By.id("ENRL_REQ_DETAIL_OVRD_UNIT_LOAD$0");
	public static By quick_TimeConflict = By.id("ENRL_REQ_DETAIL_OVRD_TIME_CNFLCT$0");
	public static By quick_RequirementDesignation = By.id("ENRL_REQ_DETAIL_OVRD_RQMNT_DESIG$0");
	public static By quick_Career = By.id("ENRL_REQ_DETAIL_OVRD_CAREER$0");
	public static By quick_ServiceIndictor = By.id("ENRL_REQ_DETAIL_OVRD_SRVC_INDIC$0");
	public static By quick_Requisites = By.id("ENRL_REQ_DETAIL_OVRD_REQUISITES$0");
	public static By classOverrides = By.xpath("//span[contains(text(), 'Class Overrides')]");
	public static By quick_ClosedClass = By.id("ENRL_REQ_DETAIL_OVRD_CLASS_LIMIT$0");
	public static By quick_ClassLinks = By.id("ENRL_REQ_DETAIL_OVRD_CLASS_LINKS$0");
	public static By quick_ClassUnits = By.id("ENRL_REQ_DETAIL_OVRD_CLASS_UNITS$0");
	public static By quick_GradingBasics = By.id("ENRL_REQ_DETAIL_OVRD_GRADING_BASIS$0");
	public static By quick_ClassPermission = By.id("ENRL_REQ_DETAIL_OVRD_CLASS_PRMSN$0");
	public static By quick_DynamicDates = By.id("DERIVED_SR_ENRL_SSR_DYND_LDRP_OVRD$0");
	public static By quick_WaitListOkay = By.id("ENRL_REQ_DETAIL_WAIT_LIST_OKAY$0");
	public static By quickEnroll_Submit = By.xpath("//input[@value='Submit']");
	public static By quickEnroll_Save = By.xpath("//input[@value='Save']");


	public static By Add_enrollment_button = By.id("ENRL_REQ_DETAIL$new$0$$0");
	public static By others_classinfo_tab = By.xpath("//span[contains(text(), 'Other Class Info')]");
	public static By WL_pos_filed = By.id("ENRL_REQ_DETAIL_CHG_TO_WL_NUM$0");
	public static By Action_dropdown2 = By.id("ENRL_REQ_DETAIL_ENRL_REQ_ACTION$1");
	public static By Enroll_ClassNbr2 = By.id("ENRL_REQ_DETAIL_CLASS_NBR$1");
	public static By Wl_pos2_link = By.id("ENRL_REQ_WRK_WAITLIST_POS_PB$1");
	public static By waitlist_pos2_text = By.id("ENRL_REQ_WRK_WAITLIST_POS$1");
	
	
	public static By changeToClassNumber=By.xpath("//input[@id='CHANGE_TO$0']");

	
	public static By eventRegister_ServiceOperation= By.id("SCC_INT_EVT_PUB_IB_OPERATIONNAME");
	public static By eventRegister_Search= By.id("#ICSearch");
	public static By eventRegistry_NOTIFYMEMBERSHIP=By.id("SCC_INT_EVT_PUB_IB_OPERATIONNAME");
	public static By eventRegistry_RunNow=By.id("SCC_RUN_BUTTON$0");
	public static By eventRegister_Save= By.xpath("//input[@value='Save']");
	
	public static By psft_SARC = By.linkText("Set Up SACR");
	public static By psft_Sys_Admin = By.linkText("System Administration");
	public static By psft_Integrations = By.linkText("Integrations");
	public static By psft_Event_Register = By.linkText("Event Register");
	public static By D2L_Classlist = By.linkText("Classlist");
}
