package com.umuc.serenity.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import com.umuc.serenity.objectRepository.psftObjectRepository;
import com.umuc.serenity.utils.SeleniumUtils;

public class CreateAndVerifyPage{
	//WebDriver driver = null;
	private By loginBtn = By.xpath("//button[@type='submit' and text()='LOG IN']");

	private static By userName = By.xpath("//input[@id='userid']");//By.id("username"); //By.xpath("//*[@id='username']");
	
	private static By psftUserName = By.xpath("//input[@id='userid']");//By.id("userid"); //By.xpath("//*[@id='username']"); //input[@id="userid"]
	
	private static By psftPwd = By.id("pwd"); //By.xpath("//*[@id='password']");//*[@id="pwd"]
	
	private static By pwd = By.xpath("//*[@id='pwd']");//By.id("password"); //By.xpath("//*[@id='password']");
	
	private By errorMsg = By.xpath("h3[contains(text(),'LOGIN ERROR')]");
	
	private By submitBtn = By.xpath("//button[@type='submit' and name = 'submit']");
	
	BasePage basePage;
	
public void enterUserName(String userNameText){
		
		SeleniumUtils.enterText(basePage.driver, userName, userNameText);
	}
	
	public void enterPassword(String pwdText){
		SeleniumUtils.enterText(basePage.driver, pwd, pwdText);
	}
	
	public void clickLoginButton() {
		SeleniumUtils.safeclick(basePage.driver, loginBtn);
	}
	
	public void enterPSFTUserName(String userNameText){
		SeleniumUtils.safeclick(basePage.driver, psftUserName);
		SeleniumUtils.enterText(basePage.driver, psftUserName, userNameText);
	}
	
	public void enterPSFTPassword(String pwdText){
		SeleniumUtils.safeclick(basePage.driver, psftPwd);
		SeleniumUtils.enterText(basePage.driver, psftPwd, pwdText);
	}
	
	public void clickSubmitButton() {
		SeleniumUtils.safeclick(basePage.driver, submitBtn);
	}
	
	public void clickStaffAndHRSALinks()
	{
		SeleniumUtils.safeclick(basePage.driver, psftObjectRepository.psft_Main_Menu);
		SeleniumUtils.safeclick(basePage.driver, psftObjectRepository.psft_toStaffPortal);
		SeleniumUtils.safeclick(basePage.driver, psftObjectRepository.psft_Hrsa_Administrator);
	}

}
