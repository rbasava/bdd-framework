package com.umuc.serenity.pages;

import ch.lambdaj.function.convert.Converter;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;

import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.firefox.internal.ProfilesIni;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBys;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import static org.hamcrest.CoreMatchers.*;

import com.umuc.serenity.utils.SeleniumUtils;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.validation.constraints.AssertFalse;
import javax.validation.constraints.AssertTrue;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import net.serenitybdd.core.pages.PageObject;

import com.umuc.serenity.objectRepository.psftObjectRepository;

/*
 * This class represents all the common elements on the website being tested.
 * 
 * The pattern being used here is called the Page Object pattern. This means that you separate
 * the code into Steps classes (LandingPageSteps.java) and Pages (LandingPage.java). All the 
 * elements on the page that you want to test go into the Pages, and all the test steps go in
 * the Steps.
 * 
 * This makes it easy to separate concerns - the Page knows what is contained on the page itself
 * and the Steps know how to test the page. 
 */
public abstract class BasePage extends PageObject {
	WebDriver driver = null;
	@FindBy(css = "img[src='images/umuc-logo-hybridstacked.svg']")
	private WebElementFacade umucLogo;

	@FindBy(css = "div.fxg-user-options__option.fxg-user-options__search-btn")
	private WebElementFacade searchLink;

	@FindBy(xpath = "//input[@name='trackingNumber']")
	private WebElementFacade trackingNumberText;
	
	private By loginBtn = By.xpath("//button[@type='submit' and text()='LOG IN']");

	private static By userName = By.xpath("//input[@id='userid']");//By.id("username"); //By.xpath("//*[@id='username']");
	
	private static By psftUserName = By.xpath("//input[@id='userid']");//By.id("userid"); //By.xpath("//*[@id='username']"); //input[@id="userid"]
	
	private static By psftPwd = By.id("pwd"); //By.xpath("//*[@id='password']");//*[@id="pwd"]
	
	private static By pwd = By.xpath("//*[@id='pwd']");//By.id("password"); //By.xpath("//*[@id='password']");
	
	private By errorMsg = By.xpath("h3[contains(text(),'LOGIN ERROR')]");
	
	private By submitBtn = By.xpath("//input[@type='submit' and @name = 'submit']");
	
	public String classNbr = "";
	
	public String classDesc = "";
	
	public String classSection = "";
	
	public BasePage(WebDriver driver) {
		super(driver);
		this.driver = driver;
		getDriver().manage().window().maximize();

	}

	public void dump(WebElement element) {
		String contents = (String) ((JavascriptExecutor) this.getDriver())
				.executeScript("return arguments[0].innerHTML;", element);
		System.out.println(contents);
	}

	public void sleep(long duration) {
		try {
			Thread.sleep(duration);
		} catch (InterruptedException e) {
			System.out.println("----------Sleep interrupted----------");
		}
	}

	// The below method is to verify the url once the user is navigated to
	// internal links
	public Boolean internalLinksNavigation(String url, String[] urlFromApplication) {
		String[] urlFromCSV = url.split(",");
		if (urlFromCSV.equals(urlFromApplication))
			return true;
		else
			return false;
	}

	// The below method is used to enter the text in Tracking Number field
	public void enterTrackingNumber(String number) {
		SeleniumUtils.safeclearandtype(driver, trackingNumberText, number);
	}

//	// The below method is used to click on the Track button
//	public void clickTrackingButton() {
//		SeleniumUtils.safeclick(driver, trackBtn);
//	}
	
	///UMUC Code
	
	public void enterUserName(String userNameText){
		
		SeleniumUtils.enterText(driver, psftObjectRepository.d2l_UserName, userNameText);
	}
	
	public void enterPassword(String pwdText){
		SeleniumUtils.enterText(driver, psftObjectRepository.d2l_UserPwd, pwdText);
	}
	
	public void clickLoginButton() {
		SeleniumUtils.safeclick(driver, loginBtn);
	}
	
	public void enterPSFTUserName(String userNameText){
		//SeleniumUtils.safeclick(driver, psftUserName);
		SeleniumUtils.enterText(driver, psftUserName, userNameText);
	}
	
	public void enterPSFTPassword(String pwdText){
		//SeleniumUtils.safeclick(driver, psftPwd);
		SeleniumUtils.enterText(driver, psftPwd, pwdText);
	}
	
	public void clickSubmitButton() {
		SeleniumUtils.safeclick(driver, submitBtn);
	}
	
	public void clickStaffAndHRSALinks()
	{
		SeleniumUtils.safeclick(driver, psftObjectRepository.psft_Main_Menu);
		SeleniumUtils.safeclick(driver, psftObjectRepository.psft_toStaffPortal);
		SeleniumUtils.safeclick(driver, psftObjectRepository.psft_Hrsa_Administrator);
	}
	
	public void navigateToScheduleNewCourse()
	{
		//String winHandleBefore = driver.getWindowHandle();

		// Perform the click operation that opens new window
		// Switch to new window opened
		for(String winHandle : driver.getWindowHandles()){
		    driver.switchTo().window(winHandle);
		}
		
		
		SeleniumUtils.safeclick(driver, psftObjectRepository.psft_Curr_Mgmt);
		SeleniumUtils.threadWait(driver);
		//SeleniumUtils.safeclick(driver, psftObjectRepository.psft_Schedule_Classes);
		
		SeleniumUtils.switchToFrameByLocator(driver, By.id("ptifrmtgtframe"));
		//System.out.println("Frame identified");
		SeleniumUtils.ExplicitWaitOnElementToBeClickable(driver, psftObjectRepository.psft_Schedule_New_Course);
		SeleniumUtils.safeclick(driver, psftObjectRepository.psft_Schedule_New_Course);
		
		//SeleniumUtils.switchToFrameByLocator(driver, By.xpath("//div[@id='ptifrmtarget']/iframe"));
		SeleniumUtils.threadWait(driver);
		//SeleniumUtils.switchToDefaultFrame(driver);
	}
	
	public void enterSearchCriteia(String term, String subArea, String catalogNbr, String campus,
			 String courseId){
		//SeleniumUtils.ExplicitWaitOnElementToBeClickable(driver, psftObjectRepository.term);
		SeleniumUtils.switchToFrameByLocator(driver, By.id("ptifrmtgtframe"));
		SeleniumUtils.safeclick(driver,  By.id("win0divSEARCHABOVE"));
		SeleniumUtils.ExplicitWaitOnElementToBeClickable(driver, psftObjectRepository.term);
		SeleniumUtils.enterText(driver, psftObjectRepository.term, term);
		SeleniumUtils.enterText(driver, psftObjectRepository.subject_area, subArea);
		SeleniumUtils.enterText(driver, psftObjectRepository.catalog_number, catalogNbr);
		SeleniumUtils.enterText(driver, psftObjectRepository.campus, campus);
		SeleniumUtils.hitKey(driver, psftObjectRepository.campus, Keys.TAB);
		SeleniumUtils.enterText(driver, psftObjectRepository.CourseID, courseId);
		SeleniumUtils.hitKey(driver, psftObjectRepository.CourseID, Keys.TAB);
					
	}
	
	public String clickClassSearchButton() {
		//SeleniumUtils.switchToFrameByLocator(driver, By.id("ptifrmtgtframe"));
		SeleniumUtils.safeclick(driver, psftObjectRepository.search_button);
		SeleniumUtils.threadWait(driver);
		return this.classNbr = SeleniumUtils.getText(driver, psftObjectRepository.classNbr) ;
		
		
	}
	
	public void clickClassSaveButton() {
		SeleniumUtils.safeclick(driver, psftObjectRepository.saveClass);
		
	}
	
	public void clickPSFTLogout() {
		SeleniumUtils.switchToDefaultFrame(driver);
		SeleniumUtils.safeclick(driver, psftObjectRepository.lnkLogOutOrSingOutInPSFT);
		
	}
	
	public void launchD2L(String url){
		SeleniumUtils.launchURL(driver, url);
	}
	
	public void loginD2L(){
		SeleniumUtils.safeclick(driver, psftObjectRepository.d2l_Login_Button);
	}
	
	public void clickSearchD2LClass(){
		SeleniumUtils.safeclick(driver, psftObjectRepository.d2l_course_search);
	}
	
	public void SearchAndVerifyD2LClass(String subjectArea, String catalog, String classSection, String term){
		SeleniumUtils.enterText(driver, psftObjectRepository.txtSearch, 
				subjectArea + " " + catalog + " " + classSection + " " + term);
		SeleniumUtils.safeclick(driver, psftObjectRepository.btnSearch);
		SeleniumUtils.scrollToElement(driver, psftObjectRepository.lnkClassNameWithClassDetails(subjectArea, catalog, classSection, term));
		SeleniumUtils.threadWait(driver);
		SeleniumUtils.safeclick(driver, psftObjectRepository.lnkClassNameWithClassDetails(subjectArea, catalog, classSection, term));
		SeleniumUtils.threadWait(driver);
	}
	
		
	public void navigateToEnrollStudent()
	{
		//String winHandleBefore = driver.getWindowHandle();

		// Perform the click operation that opens new window
		// Switch to new window opened
		for(String winHandle : driver.getWindowHandles()){
		    driver.switchTo().window(winHandle);
		}
				
		SeleniumUtils.safeclick(driver, psftObjectRepository.psft_Rec_And_Enroll);
		SeleniumUtils.threadWait(driver);
		
		SeleniumUtils.switchToFrameByLocator(driver, By.id("ptifrmtgtframe"));
		
		SeleniumUtils.ExplicitWaitOnElementToBeClickable(driver, psftObjectRepository.psft_Qucik_Enroll);
		SeleniumUtils.safeclick(driver, psftObjectRepository.psft_Qucik_Enroll);	
		
		SeleniumUtils.threadWait(driver);
		
	}
	
	public void searchAndAddStudent(String term, String studentID, String acadCareer){
		//SeleniumUtils.ExplicitWaitOnElementToBeClickable(driver, psftObjectRepository.term);
		SeleniumUtils.switchToFrameByLocator(driver, By.id("ptifrmtgtframe"));
		//SeleniumUtils.safeclick(driver,  By.id("win0divSEARCHABOVE"));
		SeleniumUtils.ExplicitWaitOnElementToBeClickable(driver, psftObjectRepository.psft_Term);
		SeleniumUtils.enterText(driver, psftObjectRepository.psft_studentId, studentID);
		SeleniumUtils.enterText(driver, psftObjectRepository.psft_AcademicCarrier, acadCareer);
		SeleniumUtils.enterText(driver, psftObjectRepository.psft_Term, term);
		SeleniumUtils.safeclick(driver, psftObjectRepository.psft_Add);					
	}
	
	public void saveOverrideDetails(String classNbr){
		
		//General Overrides
		SeleniumUtils.ExplicitWaitOnElementToBeClickable(driver, psftObjectRepository.quickEnroll_ClassNbr);
		SeleniumUtils.enterText(driver, psftObjectRepository.quickEnroll_ClassNbr, classNbr);
		SeleniumUtils.hitKey(driver, psftObjectRepository.quickEnroll_ClassNbr, Keys.TAB);
		SeleniumUtils.threadLongWait(driver);
		SeleniumUtils.threadSmallWait(driver);
		SeleniumUtils.safeclick(driver, psftObjectRepository.generalOverrides);
		SeleniumUtils.threadWait(driver);
		SeleniumUtils.ExplicitWaitOnElementToBeClickable(driver, psftObjectRepository.quick_RequirementDesignation);
		SeleniumUtils.safeclick(driver, psftObjectRepository.quick_RequirementDesignation);
		SeleniumUtils.threadVerySmallWait(driver);
		SeleniumUtils.safeclick(driver, psftObjectRepository.quick_UnitLoad);
		SeleniumUtils.threadVerySmallWait(driver);
		SeleniumUtils.safeclick(driver, psftObjectRepository.quick_Appointment);
		SeleniumUtils.threadVerySmallWait(driver);
		SeleniumUtils.safeclick(driver, psftObjectRepository.quick_TimeConflict);
		SeleniumUtils.threadVerySmallWait(driver);
		SeleniumUtils.safeclick(driver, psftObjectRepository.quick_Career);
		SeleniumUtils.threadVerySmallWait(driver);
		SeleniumUtils.safeclick(driver, psftObjectRepository.quick_ServiceIndictor);
		SeleniumUtils.threadVerySmallWait(driver);
		SeleniumUtils.safeclick(driver, psftObjectRepository.quick_Requisites);
		SeleniumUtils.threadVerySmallWait(driver);
		//Class Overrides
		SeleniumUtils.safeclick(driver, psftObjectRepository.classOverrides);
		SeleniumUtils.threadLongWait(driver);
		SeleniumUtils.ExplicitWaitOnElementToBeClickable(driver, psftObjectRepository.quick_ClosedClass);
		
		SeleniumUtils.safeclick(driver, psftObjectRepository.quick_ClosedClass);
		SeleniumUtils.threadVerySmallWait(driver);
		SeleniumUtils.safeclick(driver, psftObjectRepository.quick_ClassLinks);
		SeleniumUtils.threadVerySmallWait(driver);
		SeleniumUtils.safeclick(driver, psftObjectRepository.quick_ClassUnits);
		SeleniumUtils.threadWait(driver);
		SeleniumUtils.safeclick(driver, psftObjectRepository.quick_GradingBasics);
		SeleniumUtils.threadVerySmallWait(driver);
		SeleniumUtils.safeclick(driver, psftObjectRepository.quick_ClassPermission);
		SeleniumUtils.threadVerySmallWait(driver);
		SeleniumUtils.safeclick(driver, psftObjectRepository.quick_DynamicDates);
		SeleniumUtils.threadVerySmallWait(driver);
		SeleniumUtils.safeclick(driver, psftObjectRepository.quick_WaitListOkay);
		SeleniumUtils.threadVerySmallWait(driver);
		SeleniumUtils.safeclick(driver, psftObjectRepository.submit_button);
		SeleniumUtils.threadLongWait(driver);
		SeleniumUtils.safeclick(driver, psftObjectRepository.quickEnroll_Save);
		SeleniumUtils.threadLongWait(driver);
		
	}
	
	public void notifyMemberShip() {
	
		SeleniumUtils.switchToDefaultFrame(driver);	
		SeleniumUtils.ExplicitWaitOnElementToBeClickable(driver, psftObjectRepository.psft_Main_Menu);
		SeleniumUtils.safeclick(driver, psftObjectRepository.psft_Main_Menu);
		SeleniumUtils.threadSmallWait(driver);
		SeleniumUtils.safeclick(driver, psftObjectRepository.psft_SARC);
		SeleniumUtils.threadSmallWait(driver);
		SeleniumUtils.safeclick(driver, psftObjectRepository.psft_Sys_Admin);
		SeleniumUtils.threadSmallWait(driver);
		SeleniumUtils.safeclick(driver, psftObjectRepository.psft_Integrations);
		SeleniumUtils.threadSmallWait(driver);
		SeleniumUtils.safeclick(driver, psftObjectRepository.psft_Event_Register);
		SeleniumUtils.threadSmallWait(driver);
		SeleniumUtils.switchToFrameByIndex(driver, 0);
		SeleniumUtils.enterText(driver, psftObjectRepository.eventRegister_ServiceOperation, "NOTIFYMEMBERSHIP");
		SeleniumUtils.hitKey(driver, psftObjectRepository.eventRegister_ServiceOperation, Keys.TAB);
		SeleniumUtils.threadSmallWait(driver);
		SeleniumUtils.safeclick(driver, psftObjectRepository.eventRegister_Search);
		SeleniumUtils.threadWait(driver);
		SeleniumUtils.ExplicitWaitOnElementToBeClickable(driver, psftObjectRepository.eventRegistry_RunNow);
		SeleniumUtils.safeclick(driver, psftObjectRepository.eventRegistry_RunNow);
		SeleniumUtils.threadLongWait(driver);
		SeleniumUtils.safeclick(driver, psftObjectRepository.eventRegister_Save);
		SeleniumUtils.threadLongWait(driver);

	}
	
	
	public void verifyEnrolledUserInClasslist(){
		SeleniumUtils.ExplicitWaitOnElementToBeClickable(driver, psftObjectRepository.D2L_Classlist);
		SeleniumUtils.safeclick(driver, psftObjectRepository.D2L_Classlist);
		SeleniumUtils.threadWait(driver);
	}
	
	
	// The below method is to verify the url once the user is navigated to
	// SSO error page
	public Boolean ssoErrorNavigationPage(String url) {
		String Url = driver.getCurrentUrl();
		if (url.equals(Url))
			return true;
		else
			return false;
	}

	public void checkErrorPage()
	{
		SeleniumUtils.waitUntilElementVisible(driver, errorMsg);
	}
}