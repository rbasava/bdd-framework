package com.umuc.serenity;

import net.serenitybdd.jbehave.SerenityStory;

/*
 * This runs just 1 test - the name of this class _MUST MATCH_ the name of the story (the first letter of the first word of the story file may be lower case).
 */
public class EnrollAndVerifyStudentTest extends SerenityStory {

}