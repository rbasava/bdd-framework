package com.umuc.serenity.utils;


import net.serenitybdd.core.SerenitySystemProperties;
import net.serenitybdd.core.pages.WebElementFacade;

import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.*;

import com.google.common.base.Function;

import static java.util.concurrent.TimeUnit.SECONDS;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Properties;
import java.util.StringTokenizer;
import java.util.concurrent.TimeUnit;


public class SeleniumUtils {




	private static final long DEFAULT_POLLING_MILLISECONDS = 1000;

	public static String getBaseURL(){
		//return SerenitySystemProperties.getProperties().getValue(ThucydidesSystemProperty.WEBDRIVER_BASE_URL);
		return System.getProperty("webdriver.base.url");
	}

	public static boolean isAlertPresent(WebDriver driver) {
	    try {
	      driver.switchTo().alert();
	      return true;
	    } catch (NoAlertPresentException e) {
	      return false;
	    }
	 }
	
	 public static String closeAlertAndGetItsText(WebDriver driver) {
		 boolean acceptNextAlert = true;
		    try {
		      Alert alert = driver.switchTo().alert();
		      String alertText = alert.getText();
		      if (acceptNextAlert) {
		        alert.accept();
		      } else {
		        alert.dismiss();
		      }
		      return alertText;
		    } finally {
		      acceptNextAlert = true;
		    }
		  }
	 
	public static Properties prop = new Properties();
	
	public static void clickPopUp(WebDriver driver){
		try{
			driver.findElement(By.xpath(".//*[@alt='close']")).click();
			driver.findElement(By.xpath(".//*[@alt='close']")).click();
			}catch(Exception e){
				System.out.println("Popup is not displaying..");
			}
	}

	public static void waitUntilElementVisible(WebDriver driver, By selector) {

		try {
			WebDriverWait wait = new WebDriverWait(driver, 15);
			wait.until(ExpectedConditions.visibilityOfElementLocated(selector));
		} catch (Exception e) {
			System.out.println("--------Wait Until Element- Threw an exception---------");
		}
	}

	public static void ExplicitWaitOnElementToBeClickable(WebDriver driver, By locator) {
		try {
			WebDriverWait wait = new WebDriverWait(driver, 20);
			wait.until(ExpectedConditions.elementToBeClickable(locator));
		} catch (Throwable e) {
		}
	}
	
	public static void safeclick(WebDriver driver, By by) {
		try {
			(new WebDriverWait(driver, 10)).until(ExpectedConditions.elementToBeClickable(by));
			driver.findElement(by).click();
		}catch (StaleElementReferenceException sere) {
			// simply retry finding the element in the refreshed DOM
			System.out.println("Inside stale element for-"+ by.toString());
			driver.findElement(by).click();
		}
		catch (TimeoutException toe) {
			System.out.println("Element identified by " + by.toString() + " was not clickable after 10 seconds");
		}
	}

	public static void highlightElement(WebDriver driver, WebElement element) { 
		for (int i = 0; i < 1; i++) { 
			JavascriptExecutor js = (JavascriptExecutor)driver;
			js.executeScript("arguments[0].setAttribute('style', arguments[1]);", element, "border: 3px solid red;"); 

		}
	}

	public static WebElement waitForElement(WebDriver driver, final By by, long seconds) {
		Wait<WebDriver> wait = new FluentWait<WebDriver>(driver)
				.withTimeout(seconds, TimeUnit.SECONDS)
				.pollingEvery(DEFAULT_POLLING_MILLISECONDS, TimeUnit.MILLISECONDS)
				.ignoring(NoSuchElementException.class);

		try {
			return wait.until(new Function<WebDriver, WebElement>() {
				public WebElement apply(WebDriver driver) {
					return driver.findElement(by);
				}
			});
		} catch(TimeoutException ex){
			throw new TimeoutException("timed out waiting for " + by + " in " + seconds + " seconds on url " + driver.getCurrentUrl() + ", title " +  driver.getTitle(), ex);
		} catch(NoSuchElementException ex){
			throw new NoSuchElementException("cannot find by=" + by + ", url=" + driver.getCurrentUrl() + ", title=" + driver.getTitle(), ex);
		}
	}

	public static WebElement safeclick1(WebDriver driver, By by, long waitSeconds) {
		WebElement el = null;
		try {
			waitForElement(driver, by, waitSeconds);
			el = waitForClickable(driver, by, waitSeconds);

			if(el.getSize().getHeight() == 0 && el.getSize().getWidth() == 0) throw new RuntimeException("button has 0 size, cannot be clicked. by " + by + ", button " + el);

			try {
				System.out.println("clicking " + by + " on " + driver.getCurrentUrl() + " [" + driver.getTitle() + "]");
				highlightElement(driver, el);
				el.click();
			} catch(StaleElementReferenceException ex){
				System.out.println("stale element " + by + ": " + ex);
				//sleepSecs(1);
				el = waitForClickable(driver, by, waitSeconds);
				System.out.println("clicking " + by + " on " + driver.getCurrentUrl() + " [" + driver.getTitle() + "]");
				highlightElement(driver, el);
				el.click();
			}

		} catch(WebDriverException ex){

			// for debugging, log if there are multiple elements found for the locator, this might be the problem
			List<WebElement> visibleOnes = new ArrayList<>(2);
			try {
				for(WebElement e: driver.findElements(by)){
					System.out.println("found element by " + by  + ": " + e);
					if(e.isDisplayed()) visibleOnes.add(e);
				}
			} catch(WebDriverException ex1){
				if(ex1 instanceof NoSuchElementException) System.out.println("did not find an element by " + by + ": " + ex1);
			}

			if(visibleOnes.size() > 1) {
				System.out.println("*** multiple visible elements existing for " + by);
				throw ex;
			}

			// possible that this is because the button is off screen....try scrolling and try clicking again
			try {
				scrollToElement(driver, by);
			} catch(Exception ex1){ System.out.println("error trying to scroll to " + by + ": " + ex1); }


			el = waitForClickable(driver, by, waitSeconds);
			//if(el.isDisplayed()) throw new RuntimeException("button is not visible, cannot be clicked. by " + by + ", el " + el);
			if(el.getSize().getHeight() == 0 && el.getSize().getWidth() == 0) throw new RuntimeException("button has 0 size, cannot be clicked. by " + by + ", button " + el);
			System.out.println("clicking " + by + " on " + driver.getCurrentUrl() + " [" + driver.getTitle() + "]");
			highlightElement(driver, el);
			el.click();
		}
		return el;
	}

	public static WebElement waitForClickable(WebDriver driver, By by, long timeoutSeconds){
		long start = System.currentTimeMillis();
		WebDriverWait wait = new WebDriverWait(driver, timeoutSeconds, DEFAULT_POLLING_MILLISECONDS);
		try {
			WebElement el = wait.until(ExpectedConditions.elementToBeClickable(by));
			long end = System.currentTimeMillis();
			if(end-start > 5000) System.out.println("had to wait " + (end-start) + " ms for " + by + " in url '" + driver.getCurrentUrl() + "' title '" + driver.getTitle() + "'");
			return el;
		} catch(TimeoutException ex){
			String msg = "error waiting for element to be clickable by " + by + " in url '" + driver.getCurrentUrl() + "' title '" + driver.getTitle() + "'";
			System.out.println(msg);
			throw new TimeoutException(msg, ex);
		}
	}
	public static void scrollToElement(WebDriver driver, By by) {
		try {
			scrollToElement(driver, waitForElement(driver, by, 1000));
		} catch(StaleElementReferenceException ex) {
			System.out.println("stale element " + by + ": " + ex);
			//sleepSecs(3);
			scrollToElement(driver, waitForElement(driver, by, 1000));
		}

	}

	private static void scrollToElement(WebDriver driver, WebElement el) {
		System.out.println("scrolling to element "  + ": " + el);
		// possible that this is because the button is off screen....try scrolling and try clicking again
		highlightElement(driver, el);
		((JavascriptExecutor)driver).executeScript("window.scrollTo(" + el.getLocation().getX() + "," + el.getLocation().getY() + ");");

		try { Thread.sleep(250); } catch(Exception t){}
	}

	
	public static void safeclearandtype(WebDriver driver, WebElementFacade element, String text) {
		try {
			highlightElement(driver, element);
			element.clear();
			element.sendKeys(text);
		} catch(Exception ex) {
			System.out.println("Inside safe clear and type-" + element + " at [" + driver.getTitle() + "]");
			
		}

	}

	
	public static void JSclick(WebDriver driver,  WebElementFacade element) {
		highlightElement(driver, element);
		JavascriptExecutor jse = (JavascriptExecutor)driver;
		jse.executeScript("arguments[0].click();", element); 
		
	}
	

	public static void waitUntilElementAppears(WebDriver driver, By selector, String friendlyName) {

		/**try {
			WebDriverWait wait = new WebDriverWait(driver, 60);
			wait.until(ExpectedConditions.visibilityOfElementLocated(selector));
		} catch (Exception e) {
			System.out.println("--------Inside Wait Until Element- Threw an exception---------" + friendlyName);
		}**/
		try {
		Wait<WebDriver> wait = new FluentWait<WebDriver>(driver).withTimeout(60, SECONDS).pollingEvery(1, SECONDS)
				.ignoring(NoSuchElementException.class, StaleElementReferenceException.class);
		wait.until(ExpectedConditions.visibilityOfElementLocated(selector));
		} catch (Exception e) {
			System.out.println("--------Inside Wait Until Element- Threw an exception---------" + friendlyName);
		}
	}

	
	public static boolean verify_url(WebDriver driver, String url){
		System.out.println("Current url="+driver.getCurrentUrl());
		return driver.getCurrentUrl().contains(url);	
	}
	
	public static void enterText(WebDriver driver, By element, String text) {
		try {
			//highlightElement(driver, element);
			driver.findElement(element).clear();
			//element.sendKeys(text);
			driver.findElement(element).sendKeys(text);
		} catch(Exception ex) {
			System.out.println("Inside safe clear and type-" + element + " at [" + driver.getTitle() + "]");
			
		}

	}
	
	public static String getText(WebDriver driver, By element) {
		try {
			//highlightElement(driver, element);
			//driver.findElement(element);
			//element.sendKeys(text);
			return driver.findElement(element).getText();
		} catch(Exception ex) {
			System.out.println("Inside safe clear and type-" + element + " at [" + driver.getTitle() + "]");
			return "";
			
		}

	}
	
	public static void switchToDefaultFrame(WebDriver driver)  {
		//boolean flag = false;
		try {
			driver.switchTo().defaultContent();
			//flag = true;
			Thread.sleep(2000);
			//return flag;
		} catch (Exception ex) {
			ex.printStackTrace();
			//return false;
		} 
		
	}
	
	public static void switchToFrameByIndex(WebDriver driver, int index) {
		//boolean flag = false;
		try {
			driver.switchTo().frame(index);
			//Thread.sleep(200);
			
		} catch (Exception ex) {
			ex.printStackTrace();
			//return false;
		} 
	}
	
	public static void switchToFrameByLocator(WebDriver driver, By locator) {
		//boolean flag = false;
		try {
			driver.switchTo().frame(driver.findElement(locator));
			//flag = true;
			//return flag;
			
		} catch (Exception ex) {

			ex.printStackTrace();
			//return false;
		} finally {}
	}
	
	public static void hitKey(WebDriver driver, By locator, Keys keyStroke) {
		//boolean flag = false;
		try {
			//Thread.sleep(1000);
			driver.findElement(locator).sendKeys(keyStroke);
			//Thread.sleep(1000);
			//flag = true;
			//return flag;
		} catch (NoSuchElementException e) {
			//return false;
		} 
	}
	
	public static void launchURL(WebDriver driver, String url){
		
		try{
			driver.navigate().to(url);
		}
		catch(Exception ex){
			ex.printStackTrace();
		}
	}
	
	public static void threadVerySmallWait(WebDriver driver)  {
		
		try {
			//driver.switchTo().defaultContent();
			
			Thread.sleep(5000);
			
		} catch (Exception e) {
			e.printStackTrace();
			
		} 
		
	}
	
	public static void threadSmallWait(WebDriver driver)  {
		
		try {
			//driver.switchTo().defaultContent();
			
			Thread.sleep(2000);
			
		} catch (Exception e) {
			e.printStackTrace();
			
		} 
		
	}
	
	public static void threadWait(WebDriver driver)  {
		
		try {
			//driver.switchTo().defaultContent();
			
			Thread.sleep(5000);
			
		} catch (Exception e) {
			e.printStackTrace();
			
		} 
		
	}
	
	public static void threadLongWait(WebDriver driver)  {
		
		try {
			//driver.switchTo().defaultContent();
			
			Thread.sleep(7000);
			
		} catch (Exception e) {
			e.printStackTrace();
			
		} 
		
	}
	
	public void scrollingToElementofAPage(WebDriver driver, By locator) {
		try{ 
		WebElement element = driver.findElement(locator);
		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView();", element);
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
}

