node('master') {
    def date = new Date();
    def folder = date.format('MM-dd-yyyy-HH_mm_ss')


    ws(pwd() + '\\tests') {
        stage('Fetch Test SCM') {
            checkout changelog: false, poll: false, scm: [$class: 'GitSCM', branches: [
                [name: '*/master']
            ], doGenerateSubmoduleConfigurations: false, extensions: [], submoduleCfg: [], userRemoteConfigs: [
                [credentialsId: env.StashCredentialsId, url: 'ssh://git@bitbucket-ssh.uhub.biz:7999/vmlnaiqa/umuc-qa-automation.git']
            ]];
        }

        stage("Run Tests") {
			def javaHome = tool(name: 'JDK.1.8', type: 'jdk');
            withEnv(["JAVA_HOME=${javaHome}"]) {
                bat "mvn verify -Dmaven.test.failure.ignore=true"
            }
        }
    }

    ws(pwd() + '\\reports') {
        stage('Fetch Reports SCM') {
            checkout changelog: false, poll: false, scm: [$class: 'GitSCM', branches: [
				[name: '*/master']
			], doGenerateSubmoduleConfigurations: false, extensions: [[$class: 'CleanBeforeCheckout']], submoduleCfg: [], userRemoteConfigs: [
				[credentialsId: env.StashCredentialsId, url: 'ssh://git@bitbucket-ssh.uhub.biz:7999/vmlnaiqa/umuc-qa-automation-reports.git']
			]];

			// in order to be able to push, need to set user to what Jenkins runs under.
			// this only works becase the ssh keys exist on the server
			sh 'git config user.name "SYSTEM"'
            sh 'git config user.email "SYSTEM@pwvmlcim"'
            sh 'git checkout master'
			sh 'git pull'
        }
    }

    stage('Copy Files') {
        bat '( ' + env.ToolRoboCopy + ' ".\\tests\\target" ".\\reports\\web\\' + folder + '" /MIR ) ^& IF %ERRORLEVEL% LEQ 4 exit /B 0'
    }

	stage ('Commit Reports') {
		ws(pwd() + '\\reports') {
            sh 'git add *'
		    sh 'git commit -m "Adding Report run from ' + folder + '"'
			sh 'git status'
			sh "git push origin master"
		}
	}

    stage('Deploy Files') {
    writeFile file: 'deployfiles.ps1', text: '''
				$dir = "D:\\apps\\jenkins\\jobs\\umuc-qa-automation-pipeline\\workspace\\reports\\web\\"
                $latest = Get-ChildItem -Path $dir | Where-Object {$_.PSIsContainer} |  Sort-Object -Property name,ws -Descending | Select-Object -First 1
                $latest.name
                $folder = $dir + $latest
                $block = { robocopy $folder \\\\dwvmlweb01\\sites\\umuc-qa.vmldev.com\\$latest /e /MT:20 /w:1 /r:1  }
                $block2 = { robocopy $folder \\\\dwvmlweb01\\sites\\umuc-qa.vmldev.com\\latest /e /MT:20 /w:1 /r:1  }
				Invoke-Command -ScriptBlock $block
                Invoke-Command -ScriptBlock $block2'''
			bat env.ToolPowerShell + / -NonInteractive -ExecutionPolicy ByPass "& '${workspace}\\deployfiles.ps1'"/
		}
	}
