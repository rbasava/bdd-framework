This repository contains the automated functional test scripts for the FedEx project. It uses Selenium webdriver (Java binding), Jbehave, Serenity and Maven to develop the scripts.

### Prepare your environment
 1. Install Java(Jdk 1.7 or above) and Maven latest version. Ensure JAVA_HOME environment variable is set and points to your JDK installation.
 2. Install any git cli for your platform, like for Windows [example](https://msysgit.github.io/)
 3. Install Eclipse Windows version or any other preferred IDE.
 4. Clone the Fedex tests repository.
 5. Shell command to run tests from project workspace- 
mvn clean verify serenity:aggregate -Dmaven.test.failure.ignore=true
 6. Configure the Saucelabs credentials, browser capabilities and the test environment URL in serenity.properties file.
 
### Run the tests
To run locally- From the project folder, run the command from project workspace- mvn verify -Dmaven.test.failure.ignore=true -DsystemPropertiesFile=config//serenity_firefox.properties

### Test results
The detailed test reports can be seen at-  project-folder/target/site/serenity/index.html


