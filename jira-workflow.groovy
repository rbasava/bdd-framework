when 'Open', {
        'success' should: 'Passed'
    }
when 'Reopened', {
        'success' should: 'Passed'
    }

when 'Passed', {
        'failure' should: 'Reopen Issue'
    }

when 'Open', {
        'failure' should: 'Failed'
    }

when 'REOPENED', {
        'failure' should: 'Failed'
    }
when 'REOPENED', {
        'success' should: 'Passed'